function [Ksigma, Stress]=ElementGeomStiffnessMat(Lx,Ly,Em,d, B, G)
a=Lx/2;
b=Ly/2;
Ke=zeros(8);
Ksigma=zeros(8);
Stress = zeros(4,3);

for i=1:4
   
   Stress(i,:) = Em*B(:,:,i)*d;
   
   S = [Stress(i, 1) Stress(i,3) 0 0 ;
        Stress(i,3) Stress(i,2) 0 0 ;
        0 0 Stress(i,1) Stress(i,3);
        0 0 Stress(i,3) Stress(i,2) ];
    
   Ksigma = Ksigma+a*b*G(:,:,i)'*S*G(:,:,i);

end
Stress = mean(Stress);
Stress = 1/sqrt(2)*((Stress(1)-Stress(2))^2+Stress(2)^2+Stress(1)^2+6*(Stress(3)^2))^(1/2);
end