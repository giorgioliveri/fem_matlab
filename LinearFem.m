clear, close all, clc;
%INPUT PHYSICAL PROPERTIES
NXH=7;                              %NUMBER OF HORIZONTAL UNIT CELLS -1
NYH=7;                              %NUMBER OF VERTICAL UNIT CELLS -1
SH=1;                               %SIZE FACTOR
por=0.5;                            %POROSITY
R=sqrt(por*SH*SH/pi);
Lx=(NXH-1)*SH;
Ly=(NYH-1)*SH;
E=2;
nu=0.25;

%INPUT NUMERICAL PROPERTIES
numelemH=13;
NX=numelemH*SH*(NXH-1)+1;           %X NUMBER OF NODES
NY=numelemH*SH*(NYH-1)+1;           %Y NUMBER OF NODES
tol=1e-6;       %TOLERANCE FOR BOUNDING BOX CONSTRAINTS
densMin=1e-3;
%INPUT BOUNDARY CONDITIONS
forceBox=[];%0 Ly 0 Ly 2 -0.5/(NX-1)
          %tol Ly Lx-tol Ly 2 -1/(NX-1)
          %Lx Ly Lx Ly 2 -0.5/(NX-1)];
dispBox=[0 0 Lx 0 1 0;   %x1 y1 x2 y2 dof disp
         0 0 Lx 0 2 0;
         0 Ly Lx Ly 2 -1;
         0 Ly Lx Ly 1 0];

%%%%%%%%%%%%%%%
%IPUT ENDS 
%%%%%%%%%%%%%%%
%INITIALIZATION OF PARAMETERS AND EMPTY VECTORS/MATRICES
Lxe=Lx/(NX-1);                        %X ELEMENT SIZE
Lye=Ly/(NY-1);                        %Y ELEMENT SIZE
x=zeros(NX*NY,1);                     %X COORDINATES NODES
y=x;                                  %Y COORDINATES NODES
[Em, B, G]=ComputeElementMatrices(Lxe,Lye,E,nu); %COMPUTES ELEMENTS
[Ke]=ElementStiffnessMat(Lxe,Lye,Em,B); %LOCAL STIFFNESS MATRIX
%K=sparse(NX*NY*2,NX*NY*2);            %GLOBAL STIFFNESS MATRIX
%Ksigma=sparse(NX*NY*2,NX*NY*2);       %GLOBAL GEOMETRICAL STIFFNESS MATRIX
f=sparse(NX*NY*2,1);                  %GLOBAL FORCE VECTOR
u=f;                                  %GLOBAL DEFORMATION VECTOR
indexAll=1:(2*NX*NY);                 %INDEX NODES
indexFix=[];                          %INDEX FIXED NODES
in=zeros((NY-1)*(NX-1),4);            %CONNECTIVITY MATRIX
DENS=ones((NY-1)*(NX-1),1);
ub=f;
%CREATE NODES
rep=0;
for iy=1:NY
    for ix=1:NX
        rep=rep+1;
        x(rep)=(ix-1)*Lxe;
        y(rep)=(iy-1)*Lye;
    end
end
%CREATE CONNECTIVITY MATRIX
rep=0;
for iy=1:NY-1
    for ix=1:NX-1
        rep=rep+1;
        in(rep,:)=[(iy-1)*NX+ix (iy-1)*NX+ix+1 iy*NX+ix+1 iy*NX+ix];
        for ny=1:NYH                  %CHECK IF THE ELEMENT IS WITHING A
            for nx=1:NXH              %PORE; IF SO IT ASSIGN MINIMAL DENSITY
                xh=SH*(nx-1);
                yh=SH*(ny-1);
                xe=sum(x(in(rep,:)))/4;
                ye=sum(y(in(rep,:)))/4;
                if norm([xe-xh,ye-yh])<=R
                    DENS(rep)=densMin;
                end     
            end
        end
    end
end
sum(DENS==1)/((NY-1)*(NX-1))        %REAL DENSITY
%ASSEMBLE GLOBAL STIFFNESS MATRIX

rep=0;
in1=zeros(1,64);
in2=in1;
for i=1:8
    for j=1:8
        rep=rep+1;
        in1(rep)=i;
        in2(rep)=j;
    end
end

Kev=reshape(Ke,1,64);

in1v=zeros(1,size(in,1)*64);
in2v=zeros(1,size(in,1)*64);
Kv=zeros(1,size(in,1)*64);

for i=1:size(in,1)
    in3([1:2:7 2:2:8])=[2*in(i,:)-1 2*in(i,:)];
    in1v((i-1)*64+1:i*64)=in3(in1);
    in2v((i-1)*64+1:i*64)=in3(in2);
    Kv((i-1)*64+1:i*64)=DENS(i)*Kev;
    %K(in2,in2)=K(in2,in2)+DENS(i)*Ke;
end
K=sparse(in1v,in2v,Kv);
%APPLY DISPLACEMENT CONSTRAINTS
for i=1:size(dispBox,1)
    ind=2*indexAll((x>dispBox(i,1)-tol).*(y>dispBox(i,2)-tol).*(x<dispBox(i,3)+tol).*(y<dispBox(i,4)+tol)==1)-2+dispBox(i,5);
    u(ind)=dispBox(i,6);
    indexFix=[indexFix ind];
end

f=f-K*u;

%APPLY FORCES
for i=1:size(forceBox,1)
    ind=2*indexAll((x>forceBox(i,1)-tol).*(y>forceBox(i,2)-tol).*(x<forceBox(i,3)+tol).*(y<forceBox(i,4)+tol)==1)-2+forceBox(i,5);
    f(ind)=f(ind)+forceBox(i,6);
end

%SOLVE SYSTEM TO FIND DEFORMATION
indexFree=setdiff(indexAll,indexFix);
u(indexFree)=K(indexFree,indexFree)\f(indexFree);

%PLOT
figure
hold on
xn=x+u(1:2:end-1);                      %HORIZONTAL DISPLACEMENT
yn=y+u(2:2:end);                        %VERTICAL DISPLACEMENT
patch(x(in(DENS==1,:)'),y(in(DENS==1,:)'),'b')
patch(x(in(DENS<1,:)'),y(in(DENS<1,:)'),'b','FaceAlpha',0.1)
axis equal
figure
hold on
patch(xn(in(DENS==1,:)'),yn(in(DENS==1,:)'),'g')
patch(xn(in(DENS<1,:)'),yn(in(DENS<1,:)'),'g','FaceAlpha',0.1)
axis equal

%GEOMETRICAL STIFFNESS MATRIX CALCULATION
clear in3
in1v=zeros(1,size(in,1)*64);
in2v=zeros(1,size(in,1)*64);
Ksigmav=zeros(1,size(in,1)*64);
for i=1:size(in,1)
    in3([1:2:7 2:2:8])=[2*in(i,:)-1 2*in(i,:)];
    in1v((i-1)*64+1:i*64)=in3(in1);
    in2v((i-1)*64+1:i*64)=in3(in2);
    [Ks, Stress(i)] = ElementGeomStiffnessMat(Lxe,Lye,Em, u(in3), B, G); %LOCAL GEOM STIFFNESS MATRIX
    Ks = reshape(Ks, 1, 64);
    Ksigmav((i-1)*64+1:i*64)=DENS(i)*Ks;
    %Ksigma(in2,in2)=Ksigma(in2,in2)+DENS(i)*Ks;
end
Ksigma = sparse(in1v,in2v,Ksigmav);
%{
Kev=reshape(Ke,1,64);

in1v=zeros(1,size(in,1)*64);
in2v=zeros(1,size(in,1)*64);
Kv=zeros(1,size(in,1)*64);

for i=1:size(in,1)
    in3([1:2:7 2:2:8])=[2*in(i,:)-1 2*in(i,:)];
    in1v((i-1)*64+1:i*64)=in3(in1);
    in2v((i-1)*64+1:i*64)=in3(in2);
    Kv((i-1)*64+1:i*64)=DENS(i)*Kev;
    %K(in2,in2)=K(in2,in2)+DENS(i)*Ke;
end
K=sparse(in1v,in2v,Kv);
%}
%% STRESS PLOTTING 
%{
for i=1:size(in,1)
    in3([1:2:7 2:2:8])=[2*in(i,:)-1 2*in(i,:)];
    Stress(i) = GetStressPerElement(Lxe,Ly,E, nu, u(in3));
end
%}
figure;
hold on;
axis equal
patch(xn(in(DENS==1,:)'),yn(in(DENS==1,:)'),Stress(DENS==1)); 
colorbar
patch(xn(in(DENS<1,:)'),yn(in(DENS<1,:)'),Stress(DENS<1),'FaceAlpha',0.1)
colorbar
title('Von Mises Stress')
%}
%% BUCKLING ANALYSIS
nModesToCompute = 10;
Modes=zeros(length(u),nModesToCompute);
Eigenvalues = zeros(nModesToCompute);
[Modes(indexFree,:),Eigenvalues] = eigs(K(indexFree,indexFree), -Ksigma(indexFree,indexFree), nModesToCompute, 0); 
Eigenvalues = diag(Eigenvalues);

[Eigenvalues, indexSorting] = sort(Eigenvalues);
Eigenvalues
Modes = Modes(:, indexSorting);
%plot eigenvactors
%%
modetoplot =1;
scalingFactor = 10;
xmode=x+scalingFactor*Modes(1:2:end-1, modetoplot);                      %HORIZONTAL DISPLACEMENT
ymode=y+scalingFactor*Modes(2:2:end, modetoplot);                        %VERTICAL DISPLACEMENT

figure
hold on
patch(xmode(in(DENS==1,:)'),ymode(in(DENS==1,:)'),'g')
patch(xmode(in(DENS<1,:)'),ymode(in(DENS<1,:)'),'g','FaceAlpha',0.1)
axis equal



