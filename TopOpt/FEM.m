function [C,U]=FEM(ELEMENT,CONST)

[KE] = KLOCAL;              %Load element stiffness matrix
edofv=zeros(8,CONST.NEL);   %Initiate vector for degrees of freedom
Kv=zeros(CONST.NEL*64,1);   %Initiate stiffness vector for efficient assembly
for i = 1:CONST.NEL 
    edofv(1:2:7,i)=2*ELEMENT.NODES(i,:)-1;
    edofv(2:2:8,i)=2*ELEMENT.NODES(i,:);
    Kv((i-1)*64+1:i*64)=reshape(KE,1,64)*ELEMENT.DENSITY(i);
end
edof1v=reshape(kron(edofv,ones(8,1)),1,CONST.NEL*64);
edof2v=reshape(kron(edofv,ones(1,8)),1,CONST.NEL*64);
K=sparse(edof1v,edof2v,Kv); %Assemble stiffness matrix
F = sparse(2*(CONST.N),1); U = zeros(2*(CONST.N),1);

%Apply boundary condtions
F(CONST.NX+CONST.NY:CONST.NX+CONST.NY:2*CONST.N,1) = -1;
fixeddofs   = union([1:CONST.NX+CONST.NY:CONST.N],[2:CONST.NX+CONST.NY:2*CONST.N]);
alldofs     = [1:2*CONST.N];
freedofs    = setdiff(alldofs,fixeddofs);

%Solve for displacment
U(freedofs,:) = K(freedofs,freedofs) \ F(freedofs,:);      
U(fixeddofs,:)= 0;
%Determine compliance
C=U'*F;