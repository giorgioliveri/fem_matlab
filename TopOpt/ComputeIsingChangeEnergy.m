function [ DE ] = ComputeIsingChangeEnergy( VARNEW, CONST, i )
 
VARNEW(VARNEW<0.2) = -1;
 
if i > 0 && i <CONST.NX
    up = VARNEW(i+CONST.NX-1);
    down = VARNEW(length(VARNEW)-(CONST.NX-1)+i);%maybe error
    if i == 1
        left = VARNEW(CONST.NX-1);
        right = VARNEW(i+1);
    elseif i == CONST.NX-1
        left = VARNEW(i-1);
        right = VARNEW(1);
    else
        left = VARNEW(i-1);
        right = VARNEW(i+1);
    end
elseif i>((CONST.NX-1)*(CONST.NY-2)) && i < length(VARNEW)+1
    
    up = VARNEW(i-((CONST.NX-1)*(CONST.NY-2)));
    down = VARNEW(i-(CONST.NX-1));
    if i == (CONST.NX-1)*(CONST.NY-2)
        left = (VARNEW(end));
        right = VARNEW(i+1);
    elseif i == length(VARNEW)
        left = VARNEW(i-1);
        right = VARNEW(length(VARNEW)-(CONST.NX-2));
    else
        left = VARNEW(i-1);
        right = VARNEW(i+1);
        
    end
else
     up = VARNEW(i+CONST.NX-1);
    down =VARNEW(i-(CONST.NX-1));
    if any(i == CONST.NX:CONST.NX-1:length(VARNEW))
        left = VARNEW(i-1+CONST.NX-1);
        right = VARNEW(i+1);
    elseif any(i==CONST.NX-1:CONST.NX-1:length(VARNEW))
        left = VARNEW(i-1);
        right = VARNEW(i-(CONST.NX-2));
    else
        left = VARNEW(i-1);
        right = VARNEW(i+1);
    end
    
end
DE = 2*VARNEW(i)*(left+right+up+down);
 
 
 
end
 
 