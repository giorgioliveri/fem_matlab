function h=PLOTITER(NODE,ELEMENT,CONST,U,h)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%CREATE FIGURES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if sum(CONST.PlotDef+CONST.PlotDens)>0
    if length(U)==0
        figure,hold on
        axis equal, axis off
        colormap('gray')
        coorx=zeros(CONST.NEL,4);
        coory=coorx;
        col=zeros(CONST.NEL,1);
        for elem=1:CONST.NEL
            cor=ELEMENT.NODES(elem,:);
            lin=[cor];
            coorx(elem,:)=NODE.COORX(lin)';
            coory(elem,:)=NODE.COORY(lin)';
            col(elem)=-ELEMENT.DENSITY(elem)*[0.8]*CONST.PlotDens+[1.0];
        end
        size(coorx)
        size(coory)
        size(col)
        h=patch(coorx',coory',col');
    else
        U=2*U/(max([1,max(abs(U))]));
        coorx=zeros(CONST.NEL,4);
        coory=coorx;
        col=zeros(CONST.NEL,1);
        for elem=1:CONST.NEL
            cor=ELEMENT.NODES(elem,:);
            lin=[cor];
            ux=CONST.PlotDef*U(2*lin-1)';
            uy=CONST.PlotDef*U(2*lin)';
            coorx(elem,:)=NODE.COORX(lin)'+ux;
            coory(elem,:)=NODE.COORY(lin)'+uy;
            col(elem)=-ELEMENT.DENSITY(elem)*[0.8]*CONST.PlotDens+[1.0];
        end
        set(h,'xdata',coorx','ydata',coory','cdata',col')
    end
    drawnow
end