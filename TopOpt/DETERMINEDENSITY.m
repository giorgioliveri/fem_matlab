function [ELEMENT]=DETERMINEDENSITY(ELEMENT,CONST,VARNEW) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%CREATE DENSITY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Determine elemental 

ELEMENT.DENSITY=VARNEW+CONST.MINDENSITY;
ELEMENT.DENSITY(ELEMENT.DENSITY>1)=1;




