function [NODE,ELEMENT,CONST]=CREATEMESH(CONST)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%CREATE MESH
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Additional constants
CONST.NX=CONST.LX;          %Number of nodes in x direction
CONST.NY=CONST.LY;          %Number of nodes in y direction
CONST.NELX=CONST.NX-1;      %Number of elements in x direction
CONST.NELY=CONST.NY-1;      %Number of elements in y direction
CONST.N=CONST.NX*CONST.NY;  %Total number of nodes
CONST.NEL=CONST.NELX*CONST.NELY;    %Total number of elements
CONST.DX=CONST.LX/(CONST.NX);       %Distance between nodes in x direction
CONST.DY=CONST.LY/(CONST.NY);       %Distance between nodes in y direction
%Initiate vectors for labels and coordinates
NODE.LABEL=zeros(CONST.NX*CONST.NY,1);
NODE.COORX=zeros(CONST.NX*CONST.NY,1);
NODE.COORY=zeros(CONST.NX*CONST.NY,1);
ELEMENT.LABEL=zeros(CONST.NEL,1);
ELEMENT.NODES=zeros(CONST.NEL,4);
ELEMENT.COORX=zeros(CONST.NEL,1);
ELEMENT.COORY=zeros(CONST.NEL,1);
ELEMENT.DENSITY=zeros(CONST.NEL,1);
%Create nodes
iter=0;
for nx=1:CONST.NX
    for ny=1:CONST.NY
        iter=iter+1;
        NODE.LABEL(iter,:)=iter;
        NODE.COORX(iter)=CONST.DX*(nx-1);
        NODE.COORY(iter)=CONST.DY*(ny-1);
    end
end
%Create elements
iter=0;
for nelx=1:CONST.NELX
    for nely=1:CONST.NELY
        iter=iter+1;
        ELEMENT.LABEL(iter)=iter;
        ELEMENT.NODES(iter,:)=[(nelx-1)*CONST.NY+nely nelx*CONST.NY+nely nelx*CONST.NY+nely+1 (nelx-1)*CONST.NY+nely+1];
        ELEMENT.COORX(iter)=sum(NODE.COORX(ELEMENT.NODES(iter,:)))/4;
        ELEMENT.COORY(iter)=sum(NODE.COORY(ELEMENT.NODES(iter,:)))/4;
        
        if nelx==1 && nely==1
            ELEMENT.NEIGH(iter).val=[iter+1 iter+CONST.NELX];
        elseif nelx==1 && nely==CONST.NELY
            ELEMENT.NEIGH(iter).val=[iter-1 iter+CONST.NELX];
        elseif nelx==CONST.NELX && nely==1
            ELEMENT.NEIGH(iter).val=[iter+1 iter-CONST.NELX];
        elseif nelx==CONST.NELX && nely==CONST.NELY
            ELEMENT.NEIGH(iter).val=[iter-1 iter-CONST.NELX];
        elseif nelx==1
            ELEMENT.NEIGH(iter).val=[iter+1 iter-CONST.NELX iter+CONST.NELX];
        elseif nelx==CONST.NELX
            ELEMENT.NEIGH(iter).val=[iter-1 iter-CONST.NELX iter+CONST.NELX];
        elseif nely==1
            ELEMENT.NEIGH(iter).val=[iter-1 iter+1 iter+CONST.NELX];
        elseif nely==CONST.NELY
            ELEMENT.NEIGH(iter).val=[iter-1 iter+1 iter-CONST.NELX];
        else
            ELEMENT.NEIGH(iter).val=[iter-1 iter+1 iter-CONST.NELX iter+CONST.NELX];
        end
    end
end

%Element Neighbors

