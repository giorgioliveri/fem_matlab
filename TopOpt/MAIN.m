clear, close all,clc

%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%
%USER INPUT
%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%
CONST.LX=20;            %Mesh size in x direction
CONST.LY=20;            %Mesh size in y direction
CONST.MINDENSITY=1e-5;  %Density of material without material
CONST.initT=10;          %Initial temperature (related to acceptance probability)
CONST.KAPPA=0.3;        %Volume fraction
CONST.NITER=1500;       %Numer of total optimization iterations
CONST.FILENAME='RES_11';%Filename to save the data
CONST.PlotDens=1;       %Plot density
CONST.PlotDef=0;        %Plot deformed structure (scaled deformation)
CONST.PlotInter=100;    %Plot every # of iteration steps
CONST.SaveMov=1;
CONST.SaveMovInter=100;

%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%
%END USER INPUT
%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%

%Initialize mesh
[NODE,ELEMENT,CONST]=CREATEMESH(CONST);

%Run to initialize maximum energy
[ELEMENT]=DETERMINEDENSITY(ELEMENT,CONST,zeros(1,CONST.NEL));
CONST.EMAX=FEM(ELEMENT,CONST);

%Simulated annealing variables
ES=zeros(CONST.NITER,1);
VAR=zeros(CONST.NITER,CONST.NEL);
ES(1)=CONST.EMAX;               %Set initial energy
VARNEW=NEIGHBORFUNC([],CONST,ELEMENT);  %Create random initial state
VAR(1,:)=VARNEW;
dEp(1)=log(2)*CONST.initT; %increase in energy acceptance rate 0.5;
h=PLOTITER(NODE,ELEMENT,CONST,[],[]);
for iter=2:CONST.NITER          %Iterate
    tic
    CONST.T=CONST.initT*(CONST.NITER-iter)/CONST.NITER;
    %Draw new density for given variable
    [ELEMENT]=DETERMINEDENSITY(ELEMENT,CONST,VARNEW);
    %Determine compliance
    [CNEW,U]=FEM(ELEMENT,CONST);
    %Determine current mass fraction
    RHO=sum(ELEMENT.DENSITY)/CONST.NEL;
    %Determine energy according to energy function
    ENEW=CNEW;
    
    %Accept or reject new state
    dF=ENEW-ES(iter-1);    %Determine difference in energy
    PA=exp(-dF/CONST.T);      %Probability to accept
    
    if PA>rand(1)       %Accept
        VAR(iter,:)=VARNEW;       %Save variables for each state
        ES(iter)=ENEW;           %Save energy of each state\
    else                %Reject
        VAR(iter,:)=VAR(iter-1,:);   %Save variables for each state
        ES(iter)=ES(iter-1);        %Save energy for each state
    end

    %Draw new design variables randomly
    VARNEW=NEIGHBORFUNC(VAR(iter,:),CONST,ELEMENT);
    
    %Print output
    t1=toc;
    fprintf('ITER = %4d | E = %1.2e | time = %1.2e\n',iter,ES(iter),t1)
    
    %Plot iteration
    if floor(iter/CONST.PlotInter)==ceil(iter/CONST.PlotInter)
        h=PLOTITER(NODE,ELEMENT,CONST,U,h);
    end
end
save([CONST.FILENAME,'.mat'])
'here'
PLOTRES(CONST.FILENAME)
