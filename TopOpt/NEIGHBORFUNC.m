function VARNEW=NEIGHBORFUNC(VARNEW,CONST,ELEMENT)
itermax = 200;
iter=0;
IsingT = 0.01;
if length(VARNEW)==0    %Draw random initial state
    VARNEW=zeros(1,CONST.NEL);
    while sum(VARNEW)<round(CONST.KAPPA*CONST.NEL)
        VARNEW(randperm(CONST.NEL,1))=1;
    end
else                    %Pertub state randomly
    
        
        var1 = 0;
        var2 = 0;
        while (var1 == var2 && iter<itermax)
            iter = iter+1;
            el1=randperm(CONST.NEL,1);
            el2 = el1;
            DE_el1 = ComputeIsingChangeEnergy( VARNEW, CONST, el1 );
            if (exp(-DE_el1/IsingT)>rand(1)) %IF true, el1 can be flipped
                
                
                DE_el2 = 0;
                while (((VARNEW(el1) == VARNEW(el2)) || (exp(-DE_el2/IsingT)<rand(1))) && iter<itermax) 
                    iter = iter+1;
                    el2 = randperm(CONST.NEL,1);
                    DE_el2 = ComputeIsingChangeEnergy( VARNEW, CONST, el2 );
                   
                end
                
                var1 = VARNEW(el1);
                var2 = VARNEW(el2);
                VAROLD=VARNEW;
                VARNEW(el1)=VAROLD(el2);
                VARNEW(el2)=VAROLD(el1);
                
                
            end
            end
   
    
    
    
 if ~(iter<itermax)
    breakesearch =1;
 end
end
end
