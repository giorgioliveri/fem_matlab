function PLOTRES(FILENAME)
close all
load([FILENAME,'.mat'])

figure(1),hold on 
semilogy(ES,'b','linewidth',2)
set(gca,'fontsize',16)
xlabel('Iteration','fontsize',16)
ylabel('E','fontsize',16)
legend('E')
set(gcf,'color','white')
set(gca,'yscale','log')

%Pick best solution and plot
[EMIN I]=min(ES)
VARNEW=VAR(I,:)
ELEMENT.DENSITY=VARNEW;
%[ELEMENT]=DETERMINEDENSITY(ELEMENT,CONST,VARNEW);

[CNEW,U]=FEM(ELEMENT,CONST);
h=PLOTITER(NODE,ELEMENT,CONST,[],[]);
RHO=sum(ELEMENT.DENSITY)/CONST.LX/CONST.LY
title(['E = ',num2str(CNEW),'  V = ',num2str(RHO)],'fontsize',16)
%Create gif for convergence history
CONST.PlotDef=0;
if CONST.SaveMov==1
    filename = [CONST.FILENAME,'.gif'];
    for i=1:CONST.SaveMovInter:CONST.NITER
        i;

        
        ELEMENT.DENSITY=VAR(i,:);
        sum(round(ELEMENT.DENSITY(1:50)))
        if i == 1
            h=PLOTITER(NODE,ELEMENT,CONST,[],[]);
        else
            h=PLOTITER(NODE,ELEMENT,CONST,U*0,h);
        end
        title(['E = ',num2str(round(ES(i))),'  Iter = ',num2str(i)],'fontsize',16)
        drawnow
        pause(0.02)
        frame = getframe(gcf);
        im = frame2im(frame);
        [imind,cm] = rgb2ind(im,256);
        if i == 1
            imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'delaytime',0.05);
        else
            imwrite(imind,cm,filename,'gif','WriteMode','append','delaytime',0.05);
        end
    end
end