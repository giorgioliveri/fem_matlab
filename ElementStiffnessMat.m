function [Ke]=ElementStiffnessMat(Lx,Ly,Em,B)
a=Lx/2;
b=Ly/2;
Ke=zeros(8);
for i=1:4 
    Ke=Ke+a*b*B(:,:,i)'*Em*B(:,:,i);
end
end
