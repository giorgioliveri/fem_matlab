function [Em, B, G]=ComputeElementMatrices(Lx,Ly,E,nu)

a=Lx/2;
b=Ly/2;
Em=E/(1-nu^2)*[1 nu 0; nu 1 0; 0 0 (1-nu)/2];
t=[-sqrt(3)/3, sqrt(3)/3, sqrt(3)/3, -sqrt(3)/3];
s=[-sqrt(3)/3, -sqrt(3)/3, sqrt(3)/3 sqrt(3)/3];



for i=1:4
    dN1dx=-(1-t(i))/(4*a);
    dN1dy=-(1-s(i))/(4*b);
    dN2dx=(1-t(i))/(4*a);
    dN2dy=-(1+s(i))/(4*b);
    dN3dx=(1+t(i))/(4*a);
    dN3dy=(1+s(i))/(4*b);
    dN4dx=-(1+t(i))/(4*a);
    dN4dy=(1-s(i))/(4*b);
    B(:,:,i)= [dN1dx    0   dN2dx   0   dN3dx   0   dN4dx    0   ;
                0   dN1dy    0    dN2dy   0   dN3dy    0    dN4dy;
              dN1dy dN1dx dN2dy   dN2dx dN3dy dN3dx dN4dy   dN4dx];
    
    G(:,:,i) = [ dN1dx 0 dN2dx 0 dN3dx 0 dN4dx 0;
                 dN1dy 0 dN2dy 0 dN3dy 0 dN4dy 0;
                   0 dN1dx 0 dN2dx 0 dN3dx 0 dN4dx;
                   0 dN1dy 0 dN2dy 0 dN3dy 0 dN4dy];
end

end