function [ VARNEW, breaksearch ] = GetNewCandidate(VAR,CONST, IsingON, IsingT, SymmetricDesign, CheckBoardStartingConf, RefCase )
% Script made by Giorgio Oliveri. PhD student @ AMOLF. g.oliveri@amolf.nl
% This function create a new candidate solution according to symmetries,
% number of unit cells.
if nargin==6
    RefCase=0;
end

if VAR ~=0
    VAR_unit = reshape(VAR,CONST.NumElementXUnitCell*CONST.NumUnitCellX, []);
    VAR_unit = reshape(VAR_unit(1:1:CONST.NumElementXUnitCell,1:1:CONST.NumElementYUnitCell),[],1);
else
    if RefCase
        VARNEW_unit = CreateUnitCellContinuousBeam(CONST);
        breaksearch = 0;
    else
        VAR_unit = [];
    end
end

if ~RefCase
    [VARNEW_unit, breaksearch]=NEIGHBORFUNC(VAR_unit,CONST, IsingON, IsingT, SymmetricDesign);  %Create random initial state
end
if CheckBoardStartingConf
    VARNEW_unit = double((invhilb(CONST.NumElementXUnitCell) < 0));
    VARNEW_unit = reshape(VARNEW_unit, 1, []);
    VARNEW_unit(VARNEW_unit==0) = CONST.MINDENSITY;
end
VARNEW = reshape(VARNEW_unit, CONST.NumElementXUnitCell, CONST.NumElementYUnitCell);
if SymmetricDesign
    temp(:, 1:2:2*CONST.NumElementYUnitCell) = VARNEW;
    temp(:, 2:2:2*CONST.NumElementYUnitCell) = flip(VARNEW);
    VARNEW = temp;
    VARNEW = reshape(VARNEW, [],1);
else
    VARNEW = reshape(repelem(VARNEW,1,CONST.NumUnitCellX),[],1);
end
VARNEW = repmat(VARNEW, CONST.NumUnitCellY,1);

end

