
averageSpan = 30;
TruncationElem = find(ES==0,1)-1;
if isempty(TruncationElem)
    TruncationElem = length(ES);
end
ESave = ones(TruncationElem,1)*nan;
ESstd = ones(TruncationElem,1)*nan;
for i = averageSpan+1:TruncationElem
    ESave(i) = sum(ES(i-(averageSpan-1):i))/averageSpan;
    ESstd(i) = std(ES(i-(averageSpan-1):i));
end
figure;
loglog(ES(2:end));
hold on
loglog(ESave);


%%

h = errorbar(ESave,ESstd)
set(gca,'YScale','log');
set(gca, 'XScale', 'log');