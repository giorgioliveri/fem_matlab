function [ DE ] = ComputeIsingChangeEnergy( VARNEW, CONST, i, SymmetricDesign )
if nargin==3
    SymmetricDesign=0;
end

VARNEW(VARNEW<0.2) = -1;

if i > 0 && i <(CONST.NumElementXUnitCell+1)
    up = VARNEW(i+(CONST.NumElementXUnitCell+1)-1);
    down = VARNEW(length(VARNEW)-((CONST.NumElementXUnitCell+1)-1)+i);%maybe error
    if i == 1
        if SymmetricDesign
            left = VARNEW(i);
        else
            left = VARNEW((CONST.NumElementXUnitCell+1)-1);
        end
        right = VARNEW(i+1);
        
    elseif i == (CONST.NumElementXUnitCell+1)-1
        left = VARNEW(i-1);
        if SymmetricDesign
            right = VARNEW(i);
        else
            right = VARNEW(1);
        end
    else
        left = VARNEW(i-1);
        right = VARNEW(i+1);
    end
elseif i>(((CONST.NumElementXUnitCell+1)-1)*((CONST.NumElementYUnitCell+1)-2)) && i < length(VARNEW)+1
    
    up = VARNEW(i-(((CONST.NumElementXUnitCell+1)-1)*((CONST.NumElementYUnitCell+1)-2)));
    down = VARNEW(i-((CONST.NumElementXUnitCell+1)-1));
    if i == ((CONST.NumElementXUnitCell+1)-1)*((CONST.NumElementYUnitCell+1)-2)+1
        if SymmetricDesign
            left = (VARNEW(i));
        else
            left = (VARNEW(end));
        end
        right = VARNEW(i+1);
    elseif i == length(VARNEW)
        left = VARNEW(i-1);
        if SymmetricDesign
            right = VARNEW(i);
        else
            right = VARNEW(length(VARNEW)-((CONST.NumElementXUnitCell+1)-2));
        end
    else
        left = VARNEW(i-1);
        right = VARNEW(i+1);
        
    end
else
    up = VARNEW(i+(CONST.NumElementXUnitCell+1)-1);
    down =VARNEW(i-((CONST.NumElementXUnitCell+1)-1));
    if any(i == (CONST.NumElementXUnitCell+1):(CONST.NumElementXUnitCell+1)-1:length(VARNEW))
        if SymmetricDesign
            left = VARNEW(i);
        else
            left = VARNEW(i-1+(CONST.NumElementXUnitCell+1)-1);
        end
        
        right = VARNEW(i+1);
    elseif any(i==(CONST.NumElementXUnitCell+1)-1:(CONST.NumElementXUnitCell+1)-1:length(VARNEW))
        left = VARNEW(i-1);
        if SymmetricDesign
            right = VARNEW(i);
        else
            right = VARNEW(i-((CONST.NumElementXUnitCell+1)-2));
        end
    else
        left = VARNEW(i-1);
        right = VARNEW(i+1);
    end
    
end
DE = 2*VARNEW(i)*(left+right+up+down);



end

