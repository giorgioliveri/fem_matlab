% Post Processing Script for Stochastic Optimization with sub-Ising routine
% This script was made by Giorgio Oliveri PhD student @ AMOLF
% g.oliveri@amolf.nl
%% Plot Assigned Buckling vs Obtained Buckling

for i = 1:size(DATA,2)
    changes = find(diff(DATA(i).ObjectiveFunc)~=0)+1;
    RefEigen(i) = DATA(i).EigenRef;
    PrescribedBuckling(i) = DATA(i).PrescribedBucklin*RefEigen(i);
    if changes(end)>size(DATA(i).VAR,1) %% this is when the simulation has been stopped before the end
        FirstEig(i) = DATA(i).Eigenvalues(changes(end-1), 1)/RefEigen(i);
    else
        FirstEig(i) = DATA(i).Eigenvalues(changes(end), 1)/RefEigen(i);
    end
    clear changes
end
PrescribedBuckling =  PrescribedBuckling;
FirstEig = FirstEig;
linewidth = 2;
fontsize = 12;
figure
plot(unique(PrescribedBuckling), [median(reshape(FirstEig, 30, []))], 'Color', [0.8500    0.3250    0.0980], 'LineWidth', linewidth )
hold on
plot([0 3], [0 3], 'k:', 'LineWidth', 0.5*linewidth)
%plot(PrescribedBuckling, FirstEig, 'o', 'Color',  [0    0.4470    0.7410])
leg = legend('Median','Location', 'northwest');
leg.FontSize = fontsize;
xlim([0, 3])
ylim([0, 3])
pbaspect([1 1 1])
xlabel('$\hat{\lambda}$','Interpreter','latex', 'FontSize', fontsize);
ylabel('$\lambda$', 'Interpreter', 'latex', 'FontSize', fontsize);
title({'30 Optimizations per point', 'Conv Tol = 1e-5'})
set(gcf, 'Color', 'w')

%% Plot Assigned Buckling vs Obtained Buckling
%    plot with median and quartiles
percentile = prctile(reshape(FirstEig, 30, []),[25 75],1);
percentile = percentile-median(reshape(FirstEig, 30, []));
errorbar(unique(PrescribedBuckling),median(reshape(FirstEig, 30, [])),percentile(1,:),percentile(2,:), 'LineWidth', linewidth/2)
hold on
plot(unique(PrescribedBuckling), [median(reshape(FirstEig, 30, []))], 'Color', [0.8500    0.3250    0.0980], 'LineWidth', linewidth )
plot([0 3], [0 3], 'k:', 'LineWidth', 0.5*linewidth)
leg = legend('25th-75th Quantiles', 'Median','Location', 'northwest');
leg.FontSize = fontsize;
xlim([0, 3])
ylim([0, 3])
pbaspect([1 1 1])
xlabel('$\hat{\lambda}$','Interpreter','latex', 'FontSize', fontsize);
ylabel('$\lambda$', 'Interpreter', 'latex', 'FontSize', fontsize);
title({'30 Optimizations per point', 'Conv Tol = 1e-5'})
set(gcf, 'Color', 'w')




