function [Ke, Ke_empty]=ElementStiffnessMat(Lx,Ly,Em,Em_empty, B)
% Script made by Giorgio Oliveri. PhD student @ AMOLF. g.oliveri@amolf.nl
% This function calculates:
%       Element Stiffness Matrix for Material and Void Elements: Ke
%          Ke_empty

a=Lx/2; 	   %Element dimension along x
b=Ly/2;        %Element dimension along x
Ke=zeros(8);
Ke_empty=zeros(8);
%Loop over the 4 Samplign Point (Gaussian Quadrature)
for i=1:4     
    Ke=Ke+a*b*B(:,:,i)'*Em*B(:,:,i);    %Element Stiffness Matrix for Material
    Ke_empty=Ke_empty+a*b*B(:,:,i)'*Em_empty*B(:,:,i); %Element Stiffness Matrix for Void
end
end
