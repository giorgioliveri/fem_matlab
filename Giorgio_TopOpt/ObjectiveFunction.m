function [ ObjFuncVal, Eigenvalues, xmode, ymode] = ObjectiveFunction( CONST, NODES, VARNEW, h ,nModesToCompute , AssignedBuckling, Modeplotting )
% Script made by Giorgio Oliveri. PhD student @ AMOLF. g.oliveri@amolf.nl
%This is the objective function calculator. It contain FEM solver and
%calculates the Eigenmodes. This Script applied a unit load to the
%structure, calculated the stress in all the elements and compute the
%geoemtrical stiffness matrix Ks. Assemble the stiffness matrix K according
%to the element density relative to the configuration (VARNEW) and solve
%the eigenvalue problem.
if nargin==6
    Modeplotting=0;
end
load = -0.01; %Unit Load applied on the upper nodes
%definition of boxes for BC (applied force or applied deformation)
forceBox=[0 CONST.Ly 0 CONST.Ly 2 load/2/(CONST.NX-1);
    CONST.tol CONST.Ly CONST.Lx-CONST.tol CONST.Ly 2 load/(CONST.NX-1);
    CONST.Lx CONST.Ly CONST.Lx CONST.Ly 2 load/2/(CONST.NX-1)]; %x1 y1 x2 y2 dof disp
dispBox=[0 0 CONST.Lx 0 1 0;   %x1 y1 x2 y2 dof disp
    0 0 CONST.Lx 0 2 0;
    0 CONST.Ly CONST.Lx CONST.Ly 1 0];

indexAll=1:(2*CONST.NX*CONST.NY);                 %All nodes indeces
indexFix=[];                                      %Fix nodes indeces

f=sparse(CONST.NX*CONST.NY*2,1);                  %Preallocation Force vector
u=f;                                              %Preallocation displacement vector

Kev=reshape(CONST.Ke,1,64);                       %Element stiffness matrix in vector form
Kev_empty=reshape(CONST.Ke_empty,1,64);           %Void Element stiffness matrix in vector form

in1v=zeros(1,CONST.NEL*64);
in2v=zeros(1,CONST.NEL*64);
Kv=zeros(1,CONST.NEL*64);                         %Preallocation of Global Stiffness Matrix

% Assembly of Stiffness Matrix. Void elements use the Void Element
% Stiffness matrix
for i=1:size(CONST.in,1)
    in3([1:2:7 2:2:8])=[2*CONST.in(i,:)-1 2*CONST.in(i,:)];
    in1v((i-1)*64+1:i*64)=in3(CONST.in1);
    in2v((i-1)*64+1:i*64)=in3(CONST.in2);
    if VARNEW(i)>0.5
        Kv((i-1)*64+1:i*64)= VARNEW(i)*Kev;
    else
        Kv((i-1)*64+1:i*64)= VARNEW(i)*Kev_empty;
    end
        %K(in2,in2)=K(in2,in2)+DENS(i)*Ke;
end
K=sparse(in1v,in2v,Kv);
%Application of displacement constrain
for i=1:size(dispBox,1)
    ind=2*indexAll((NODES.x>dispBox(i,1)-CONST.tol).*(NODES.y>dispBox(i,2)-CONST.tol).*(NODES.x<dispBox(i,3)+CONST.tol).*(NODES.y<dispBox(i,4)+CONST.tol)==1)-2+dispBox(i,5);
    u(ind)=dispBox(i,6);
    indexFix=[indexFix ind];
end
f=f-K*u;

%Application of Force constrain
for i=1:size(forceBox,1)
    ind=2*indexAll((NODES.x>forceBox(i,1)-CONST.tol).*(NODES.y>forceBox(i,2)-CONST.tol).*(NODES.x<forceBox(i,3)+CONST.tol).*(NODES.y<forceBox(i,4)+CONST.tol)==1)-2+forceBox(i,5);
    f(ind)=f(ind)+forceBox(i,6);
end

%Solve system to find deformation
indexFree=setdiff(indexAll,indexFix);

if CONST.ForceControlled %If forceController, the load is vertical force applied to the nodes (Via Transformation Matrix)
    CONST.realMasterDof=setdiff(indexAll,CONST.slaveDoF);
    [C,IA]=setdiff(CONST.realMasterDof,indexFix);
    TKT = CONST.TransformationM(indexFree,IA)'*(K(indexFree, indexFree)*CONST.TransformationM(indexFree,IA));
    TF = (CONST.TransformationM(indexFree,IA)'*f(indexFree));
    u(indexFree) = CONST.TransformationM(indexFree,IA)*(TKT\TF);
else
    u(indexFree)=K(indexFree,indexFree)\f(indexFree);
end
f=K*u;

%Geomtrical stiffness matrix calculation
clear in3
in1v=zeros(1,size(CONST.in,1)*64);
in2v=zeros(1,size(CONST.in,1)*64);
Ksigmav=zeros(1,size(CONST.in,1)*64);

newdensity = VARNEW;
newdensity(newdensity<0.5)=0; %To remove occurrence of localized modes, when 
                              %the element density is low, the geometrical
                              %stiffness matrix terms are zeroed. 
for i=1:size(CONST.in,1)
    in3([1:2:7 2:2:8])=[2*CONST.in(i,:)-1 2*CONST.in(i,:)];
    in1v((i-1)*64+1:i*64)=in3(CONST.in1);
    in2v((i-1)*64+1:i*64)=in3(CONST.in2);
    [Ks, Stress(i)] = ElementGeomStiffnessMat(CONST.Lxe,CONST.Lye,CONST.Em, u(in3), CONST.B, CONST.G); %LOCAL GEOM STIFFNESS MATRIX
    Ks = reshape(Ks, 1, 64);
    Ksigmav((i-1)*64+1:i*64)=newdensity(i)*Ks;
end
Ksigma = sparse(in1v,in2v,Ksigmav);

%% Buckling Analysis

Modes=zeros(length(u),nModesToCompute);
Eigenvalues = zeros(nModesToCompute);


if CONST.ForceControlled
    TKsT =CONST.TransformationM(indexFree,IA)'*(Ksigma(indexFree, indexFree)*CONST.TransformationM(indexFree,IA));
    RealIndexFree=setdiff(indexFree, CONST.slaveDoF);
    [Modes(RealIndexFree,:),Eigenvalues] = eigs(TKT, -TKsT, nModesToCompute,0);
    
else
    [Modes(indexFree,:),Eigenvalues] = eigs(K(indexFree,indexFree), -Ksigma(indexFree,indexFree), nModesToCompute, 0);
end


Eigenvalues = diag(Eigenvalues);
[Eigenvalues, indexSorting] = sort(Eigenvalues);
Modes = Modes(:, indexSorting);


%% OBJECTIVE FUNCTION CALCULATION
% This is calculated by normalization of the eigenvalue respect to the
% eigenvalue of straight beam of equal total density.

ObjFuncVal = 0;
for i =1
ObjFuncVal =ObjFuncVal+((Eigenvalues(i)/abs(CONST.EigenRef))-AssignedBuckling)^2;
end
ObjFuncVal = sqrt(ObjFuncVal);
scalingFactor = 0.2; %Mode shape scaling factor respect to the mode maximum displacement
for i=1:size(Eigenvalues,1)
    maxdisplacement = max(sqrt((Modes(1:2:end-1, i)).^2+(Modes(2:2:end, i)).^2));
    xmode(:,i)=NODES.x+scalingFactor*Modes(1:2:end-1, i)/maxdisplacement;    %Horizontal displacement
    ymode(:,i)=NODES.y+scalingFactor*Modes(2:2:end, i)/maxdisplacement;      %Vertical displacement
end

%Plot eigenvalues
if Modeplotting
    modetoplot =1;
    temp_x =xmode(:,modetoplot);
    temp_y =ymode(:,modetoplot);
    set(h,'xdata',temp_x(CONST.in'),'ydata',temp_y(CONST.in'), 'cdata', -VARNEW, 'EdgeColor', 'k')
    axis equal
    title(sprintf('ObjFunc = %.4d, Eig = %.4d', ObjFuncVal, Eigenvalues(modetoplot)))
    drawnow
end
xmode = reshape(xmode, [],1);
ymode = reshape(ymode, [],1);

end

