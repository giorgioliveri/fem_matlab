# Script made by Giorgio Oliveri g.oliveri@amolf.nl
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.io as sio

data = sio.loadmat('BiAxial_DATA_Tising=[99999.00-99999.00]_TtopOpt=[0.00-0.02].mat')



#>>> mat_contents = sio.loadmat('octave_struct.mat')
data = data['DATA'];
val = data[0,0]


'''
>>> val
([[1.0]], [[2.0]])
>>> val['field1']
array([[ 1.]])
>>> val['field2']
array([[ 2.]])
>>> val.dtype
dtype([('field1', 'O'), ('field2', 'O')])
'''