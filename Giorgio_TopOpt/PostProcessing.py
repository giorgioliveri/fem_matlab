import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio

#%%
Directory = ('BiAxial_IsingTvsTopOptT_temperatureStudy/Tising=[0.20-1.20]_TtopOpt=[0.00-0.08]/')
DatasetName = ('4x4_BiAxial_DATA_Tising=[0.20-1.20]_TtopOpt=[0.00-0.08]')
data = sio.loadmat(Directory+DatasetName);
data =data['DATA']  # variable in mat file
mdtype = data.dtype  # dtypes of structures are "unsized objects"

#%%
TopOptTempRange = np.array([]);
IsingTempRange = np.array([]);
TotIsingEnergy = np.array([]);
IsingStopped = np.array([]);
OptimalSolution = np.array([]);
nsample = data['ObjectiveFunc'][0][0].shape[1];

for i in np.arange(0,data['TopOptTemperature'][0][:].size):
    if data['TopOptTemperature'][0][i][0].size==0:
        break;
    else:
        TopOptTempRange = np.append(TopOptTempRange,data['TopOptTemperature'][0][i][0][0] )
        IsingTempRange = np.append(IsingTempRange, data['IsingTemperature'][0][i][0][0] )

MinEnergy = np.zeros([IsingTempRange.size, nsample])

for index in np.arange(0, IsingTempRange.size):
    TotIsingEnergy = np.append(TotIsingEnergy, np.mean(data['TotIsingEnergy'][0][index]));
    IsingStopped = np.append(IsingStopped, np.mean(data['IsingStopped'][0][index]))
    OptimalSolution = np.append(OptimalSolution, np.mean(data['OptimalSolution'][0][index]))
    for sample in np.arange(0, nsample):
        #data['ObjectiveFunc'][0][index][:,sample]
        indexZero = np.where(data['ObjectiveFunc'][0][index][:,sample]==0)[0];
        
        if any(indexZero):
            indexMin = indexZero[0]-1;
            indexZero = [];
            MinEnergy[index,sample] = data['ObjectiveFunc'][0][index][indexMin,sample]
            

        else:
            MinEnergy[index,sample] = data['ObjectiveFunc'][0][index][-1,sample]
            
#%% Cutting because missing data

indexToCut = np.where(IsingTempRange==IsingTempRange[-1])[0][0] ;  

MinEnergy = MinEnergy[:indexToCut,:];     

TotIsingEnergy = TotIsingEnergy[:indexToCut];
IsingStopped =IsingStopped[:indexToCut]
OptimalSolution = OptimalSolution[:indexToCut]

    
    
#%% CONTOUR PLOT - Tising vs TtopOpt vs ObjFunc
# for the final version i need to remove the CUT in ISING TEMP RANGE

MeanMinEnergy = np.reshape(np.mean(MinEnergy,1), (np.unique(IsingTempRange[:indexToCut]).size,np.unique(TopOptTempRange).size ))
plt.contourf(np.unique(IsingTempRange[:indexToCut]),np.unique(TopOptTempRange), np.transpose(MeanMinEnergy))
plt.colorbar()
plt.xlabel('$T_{ising}$')
plt.ylabel('$T_{TopOpt}$')
plt.title('Mean Min Obj Func value for %d sample' %nsample)

#%% CONTOUR PLOT - Tising vs TtopOpt vs TotalIsingEnergy

TotIsingEnergy = np.reshape(TotIsingEnergy, (np.unique(IsingTempRange[:indexToCut]).size,np.unique(TopOptTempRange).size ))
plt.contourf(np.unique(IsingTempRange[:indexToCut]),np.unique(TopOptTempRange), np.transpose(TotIsingEnergy));
plt.colorbar()
plt.xlabel('$T_{ising}$')
plt.ylabel('$T_{TopOpt}$')
plt.title('Mean Total Ising Energy value for %d sample (Final State)' %nsample);

#%% CONTOUR PLOT - Tising vs TtopOpt vs ISING STOPPED
IsingStopped = np.reshape(IsingStopped, (np.unique(IsingTempRange[:indexToCut]).size,np.unique(TopOptTempRange).size ))
plt.contourf(np.unique(IsingTempRange[:indexToCut]),np.unique(TopOptTempRange), np.transpose(IsingStopped));
plt.colorbar()
plt.xlabel('$T_{ising}$')
plt.ylabel('$T_{TopOpt}$')
plt.title('Ising Stopped for %d sample (Final State)' %nsample);

#%% CONTOUR PLOT - Tising vs TtopOpt vs OptimalSolution
OptimalSolution = np.reshape(OptimalSolution, (np.unique(IsingTempRange[:indexToCut]).size,np.unique(TopOptTempRange).size ))
plt.contourf(np.unique(IsingTempRange[:indexToCut]),np.unique(TopOptTempRange), np.transpose(OptimalSolution));
plt.colorbar()
plt.xlabel('$T_{ising}$')
plt.ylabel('$T_{TopOpt}$')
plt.title('Optimal Solution (TopOpt) for %d sample (Final State)' %nsample);

#%% Plot Tising vs ObjFunction for different TopOpt
TopOptTemperatureToPlot = [0.01, 0.0175, 0.035]

for value in TopOptTemperatureToPlot:
    index = np.where(np.isclose(np.unique(TopOptTempRange),value)==True)[0]
    plt.plot(np.unique(IsingTempRange[:indexToCut]),MeanMinEnergy[:,index], label='$T_{TopOpt}=%.3f$' %value)
    
plt.legend()

#%% Mergin data

IsingStopped_FirstPart = IsingStopped;
IsingTempRange_FirstPart = IsingTempRange;
MeanMinEnergy_FirstPart = MeanMinEnergy;
MinEnergy_FirstPart = MinEnergy;
OptimalSolution_FirstPart = OptimalSolution;
TopOptTempRange_FirstPart = TopOptTempRange;
TotIsingEnergy_FirstPart = TotIsingEnergy;
indexToCut_FirstPart = indexToCut;


    
    






