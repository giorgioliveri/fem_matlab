function [ UnitCell ] = CreateUnitCellContinuousBeam( CONST)
%This function create unit cell for which, a symmetric beam of materials
% of the assigned density can be created. If a complete beam can not be done,
% the material is assigned at the centre of the unit cell. This ONLY work
% for square unit cells

UnitCell = zeros(1,CONST.NumIndepentElem)+CONST.MINDENSITY;

fraction = size(UnitCell,2)/CONST.NumElementYUnitCell*CONST.Porosity
% fraction = (sqrt(size(UnitCell,2))*CONST.Porosity);
intere = floor(fraction);
rounding = fraction-intere;

for i =1:intere
   UnitCell(CONST.NumElementXUnitCell-(i-1):CONST.NumElementXUnitCell:end)=1;
end

if rounding
    numcelltoadd = round(rounding*CONST.NumElementYUnitCell);
    UnitCell(CONST.NumElementXUnitCell-intere:CONST.NumElementXUnitCell:CONST.NumElementXUnitCell*numcelltoadd-intere)= 1;
    
end


end

