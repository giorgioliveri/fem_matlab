%% Video Making Script for APS March Meetings 2018
%%

%%

IndexToPlot = 1;
patchdistance = 4;
fontsize = 30;


fid = figure;
fid.PaperUnits = 'points';
%print('ScreenSizeFigure','-dpng','-r0')
width=1920;
height=1080;
set(fid,'units','points','position',[0,0,width,height])
hold on
colormap('gray')
col = ones(CONST.NEL, 1);
axis equal
patch_state =patch(NODES.x(CONST.in'),NODES.y(CONST.in'), -col');
patch_mode1 =patch(NODES.x(CONST.in')+patchdistance,NODES.y(CONST.in'), -col');
patch_mode2 =patch(NODES.x(CONST.in')+2*patchdistance,NODES.y(CONST.in'), -col');
patch_mode3 =patch(NODES.x(CONST.in')+3*patchdistance,NODES.y(CONST.in'), -col');
xlim([-2, 4*patchdistance])
text(1, -0.5, 'Config', 'FontWeight', 'bold', 'HorizontalAlignment', 'center', 'FontSize', fontsize);
text(1+patchdistance, -0.5, 'Mode 1', 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
text(1+2*patchdistance, -0.5, 'Mode 2', 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
text(1+3*patchdistance, -0.5, 'Mode 3', 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
set(gcf, 'Color', 'w')
text(8, 11, sprintf('Assigned \\lambda = %.2f', DATA(IndexToPlot).PrescribedBucklin), 'FontWeight', 'bold','HorizontalAlignment', 'center', 'FontSize', fontsize)
axis off
%% Set up the movie.
writerObj = VideoWriter(sprintf('ModeShapes_OptmizationFirstMode_%d.avi',IndexToPlot )); % Name it.
writerObj.FrameRate = 8; % How many frames per second.
writerObj.Quality = 100;
open(writerObj);

changes = find(diff(DATA(IndexToPlot).ObjectiveFunc)~=0)+1;
for fakeindex=1:size(changes,1)-1
    i = changes(fakeindex);
    % We just use pause but pretend you have some really complicated thing here...
    pause(0.1);
    figure(fid); % Makes sure you use your desired frame.
    
    set(patch_state,'xdata',NODES.x(CONST.in'),'ydata',NODES.y(CONST.in'), 'cdata', -DATA(IndexToPlot).VAR(i,:)', 'EdgeColor', 'k')
    xmode = reshape((DATA(IndexToPlot).xmode(:,i)), [], 5);
    ymode = reshape((DATA(IndexToPlot).ymode(:,i)), [], 5);
    temp_x =xmode(:,1);
    temp_y =ymode(:,1);
    set(patch_mode1,'xdata',temp_x(CONST.in')+patchdistance,'ydata',temp_y(CONST.in'), 'cdata', -DATA(IndexToPlot).VAR(i,:)', 'EdgeColor', 'k')
    
    temp_x =xmode(:,2);
    temp_y =ymode(:,2);
    set(patch_mode2,'xdata',temp_x(CONST.in')+2*patchdistance,'ydata',temp_y(CONST.in'), 'cdata', -DATA(IndexToPlot).VAR(i,:)', 'EdgeColor', 'k')
    
    temp_x =xmode(:,3);
    temp_y =ymode(:,3);
    set(patch_mode3,'xdata',temp_x(CONST.in')+3*patchdistance,'ydata',temp_y(CONST.in'), 'cdata', -DATA(IndexToPlot).VAR(i,:)', 'EdgeColor', 'k')
    if fakeindex ==1
        h = text(1+patchdistance, 10.4, sprintf('\\lambda= %.5f', DATA(IndexToPlot).Eigenvalues(i,1)/8.9338), 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
        g = text(1+2*patchdistance, 10.4, sprintf('\\lambda= %.5f', DATA(IndexToPlot).Eigenvalues(i,2)/8.9338), 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
        f = text(1+3*patchdistance, 10.4, sprintf('\\lambda= %.5f', DATA(IndexToPlot).Eigenvalues(i,3)/8.9338), 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
    else
        set(h,'String', sprintf('\\lambda= %.5f', DATA(IndexToPlot).Eigenvalues(i,1)/8.9338))
        set(g,'String', sprintf('\\lambda= %.5f', DATA(IndexToPlot).Eigenvalues(i,2)/8.9338))
        set(f,'String', sprintf('\\lambda= %.5f', DATA(IndexToPlot).Eigenvalues(i,3)/8.9338))
    end
    %text(1+2*patchdistance, 11, 'Mode 2', 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
    %text(1+3*patchdistance, 11, 'Mode 3', 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
    
    drawnow
    %if mod(i,1)==0, % Uncomment to take 1 out of every 4 frames.
    frame = getframe(gcf); % 'gcf' can handle if you zoom in to take a movie.
    writeVideo(writerObj, frame);
    %end
end
hold off
close(writerObj); % Saves the movie.

%% box plot
figure
line(1:3,[1,1,1])
hold on
boxplot(reshape(FirstEig, 10, []), unique(PrescribedBuckling))
%set(gca,'box','off')
hold on
h = copyobj(gca,gcf);
delete(allchild(h))
set(h,'Color','none','XtickL','','YAxisLoc','right',...
    'next','add','Ylim',[200,300])
%hold on
plot(h,[0:0.01:0.7],[0:0.01:0.7])
%plot([0:0.01:0.7], [0:0.01:0.7], 'k--')
%% Singe Mode shape Plotting

IndexToPlot =1;
fontsize = 12

fid = figure;
%fid.PaperUnits = 'points';
%width=1920;
%height=1080;
%set(fid,'units','points','position',[0,0,width,height])
hold on
colormap('gray')
col = ones(CONST.NEL, 1);
axis equal
changes = find(diff(DATA(IndexToPlot).ObjectiveFunc)~=0)+1;
patch_state =patch(NODES.x(CONST.in'),NODES.y(CONST.in'), -DATA(IndexToPlot).VAR(end, :));
patch_mode =patch(NODES.x(CONST.in')+patchdistance,NODES.y(CONST.in'), -DATA(IndexToPlot).VAR(end, :));
patch_mode1 =patch(NODES.x(CONST.in')+2*patchdistance,NODES.y(CONST.in'), -DATA(IndexToPlot).VAR(end, :));
patch_mode2 =patch(NODES.x(CONST.in')+3*patchdistance,NODES.y(CONST.in'), -DATA(IndexToPlot).VAR(end, :));
patch_mode3 =patch(NODES.x(CONST.in')+4*patchdistance,NODES.y(CONST.in'), -DATA(IndexToPlot).VAR(end, :));




axis off;
set(gcf, 'color', 'w');

if changes(end)>size(DATA(IndexToPlot).VAR,1)
    xmode = reshape((DATA(IndexToPlot).xmode(:,changes(end-1))), [], 5);
    ymode = reshape((DATA(IndexToPlot).ymode(:,changes(end-1))), [], 5);
    temp_x =xmode(:,1);
    temp_y =ymode(:,1);
    
    temp_x1 = xmode(:,2);
    temp_y1 = ymode(:,2);
    
    temp_x2 = xmode(:,3);
    temp_y2 = ymode(:,3);
    
    temp_x3 = xmode(:,4);
    temp_y3 = ymode(:,4);
    
   % title(sprintf('Assigned \\lambda = %.4f  \\lambda = %.6f',DATA(IndexToPlot).PrescribedBucklin, DATA(IndexToPlot).Eigenvalues(changes(end-1), 1)/8.9338), 'FontWeight', 'bold', 'FontSize', fontsize);
    set(patch_mode,'xdata',temp_x(CONST.in')+patchdistance,'ydata',temp_y(CONST.in'), 'cdata', -DATA(IndexToPlot).VAR(changes(end-1),:)', 'EdgeColor', 'k')
    set(patch_mode1,'xdata',temp_x1(CONST.in')+2*patchdistance,'ydata',temp_y1(CONST.in'), 'cdata', -DATA(IndexToPlot).VAR(changes(end-1),:)', 'EdgeColor', 'k')
    set(patch_mode2,'xdata',temp_x2(CONST.in')+3*patchdistance,'ydata',temp_y2(CONST.in'), 'cdata', -DATA(IndexToPlot).VAR(changes(end-1),:)', 'EdgeColor', 'k')
    set(patch_mode3,'xdata',temp_x3(CONST.in')+4*patchdistance,'ydata',temp_y3(CONST.in'), 'cdata', -DATA(IndexToPlot).VAR(changes(end-1),:)', 'EdgeColor', 'k')
    
    text(1+patchdistance, 10.4, sprintf('\\lambda = %.6f',DATA(IndexToPlot).Eigenvalues(changes(end-1), 1)/8.9338), 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
    text(1+2*patchdistance, 10.4, sprintf('\\lambda = %.6f',DATA(IndexToPlot).Eigenvalues(changes(end-1), 2)/8.9338), 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
    text(1+3*patchdistance, 10.4, sprintf('\\lambda = %.6f',DATA(IndexToPlot).Eigenvalues(changes(end-1), 3)/8.9338), 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
    text(1+4*patchdistance, 10.4, sprintf('\\lambda = %.6f',DATA(IndexToPlot).Eigenvalues(changes(end-1), 4)/8.9338), 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
else
    xmode = reshape((DATA(IndexToPlot).xmode(:,changes(end))), [], 5);
    ymode = reshape((DATA(IndexToPlot).ymode(:,changes(end))), [], 5);
    temp_x =xmode(:,1);
    temp_y =ymode(:,1);
    
    temp_x1 = xmode(:,2);
    temp_y1 = ymode(:,2);
    
    temp_x2 = xmode(:,3);
    temp_y2 = ymode(:,3);
    
    temp_x3 = xmode(:,4);
    temp_y3 = ymode(:,4);
 %   title(sprintf('Assigned \\lambda = %.4f  \\lambda = %.6f',DATA(IndexToPlot).PrescribedBucklin, DATA(IndexToPlot).Eigenvalues(changes(end), 1)/8.9338), 'FontWeight', 'bold', 'FontSize', fontsize);
    set(patch_mode,'xdata',temp_x(CONST.in')+patchdistance,'ydata',temp_y(CONST.in'), 'cdata', -DATA(IndexToPlot).VAR(changes(end),:)', 'EdgeColor', 'k')
   set(patch_mode1,'xdata',temp_x1(CONST.in')+2*patchdistance,'ydata',temp_y1(CONST.in'), 'cdata', -DATA(IndexToPlot).VAR(changes(end),:)', 'EdgeColor', 'k')
   set(patch_mode2,'xdata',temp_x2(CONST.in')+3*patchdistance,'ydata',temp_y2(CONST.in'), 'cdata', -DATA(IndexToPlot).VAR(changes(end),:)', 'EdgeColor', 'k')
   set(patch_mode3,'xdata',temp_x3(CONST.in')+4*patchdistance,'ydata',temp_y3(CONST.in'), 'cdata', -DATA(IndexToPlot).VAR(changes(end),:)', 'EdgeColor', 'k')
    
    text(1+patchdistance, 10.4, sprintf('\\lambda = %.6f',DATA(IndexToPlot).Eigenvalues(changes(end), 1)/8.9338), 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
    text(1+2*patchdistance, 10.4, sprintf('\\lambda = %.6f',DATA(IndexToPlot).Eigenvalues(changes(end), 2)/8.9338), 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
    text(1+3*patchdistance, 10.4, sprintf('\\lambda = %.6f',DATA(IndexToPlot).Eigenvalues(changes(end), 3)/8.9338), 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
    text(1+4*patchdistance, 10.4, sprintf('\\lambda = %.6f',DATA(IndexToPlot).Eigenvalues(changes(end), 4)/8.9338), 'FontWeight', 'bold', 'HorizontalAlignment', 'center','FontSize', fontsize);
end

%print(sprintf('State_and_FirstMode_lambda_%.3f_index%d.pdf', DATA(IndexToPlot).PrescribedBucklin, IndexToPlot),'-dpdf','-bestfit')
%print(sprintf('4Modes_lambda_%.3f_index%d_dataset15.pdf', DATA(IndexToPlot).PrescribedBucklin, IndexToPlot),'-dpdf','-bestfit')
%%
