function [Em,Em_empty,  B, G]=ComputeElementMatrices(Lx,Ly,E,nu)
% Script made by Giorgio Oliveri. PhD student @ AMOLF. g.oliveri@amolf.nl
% This function calculates:
%       Elasticity matrix Em for Material and Void elements
%       Strain-Displacement B matrix on 4 Sampling points (Gaussian
%       Quadrature)
%       G matrix on 4 Sampling points (Gaussian Quadrature)

a=Lx/2;                                       %Element dimension along x
b=Ly/2;                                       %Element dimension along y
Em=E/(1-nu^2)*[1 nu 0; nu 1 0; 0 0 (1-nu)/2]; %Elasticity matrix for Material element
Em_empty = E/1*[1 0 0; 0 1 0; 0 0 1/2];       %Elasticity matrix for Void element
t=[-sqrt(3)/3, sqrt(3)/3, sqrt(3)/3, -sqrt(3)/3]; % Sampling Points coordinate for Gaussian Quadrature (along x)
s=[-sqrt(3)/3, -sqrt(3)/3, sqrt(3)/3 sqrt(3)/3];  % Sampling Points coordinate for Gaussian Quadrature (along y)


%Loop over the 4 Sampling Points
for i=1:4
    % Derivates of Shape function N1, N2, N3, N4 respect x and y calculated
    % on the Sampling Point
    dN1dx=-(1-t(i))/(4*a);
    dN1dy=-(1-s(i))/(4*b);
    dN2dx=(1-t(i))/(4*a);
    dN2dy=-(1+s(i))/(4*b);
    dN3dx=(1+t(i))/(4*a);
    dN3dy=(1+s(i))/(4*b);
    dN4dx=-(1+t(i))/(4*a);
    dN4dy=(1-s(i))/(4*b);
    %Strain-Displacement Matrix
    B(:,:,i)= [dN1dx    0   dN2dx   0   dN3dx   0   dN4dx    0   ;
                0   dN1dy    0    dN2dy   0   dN3dy    0    dN4dy;
              dN1dy dN1dx dN2dy   dN2dx dN3dy dN3dx dN4dy   dN4dx];
    
    G(:,:,i) = [ dN1dx 0 dN2dx 0 dN3dx 0 dN4dx 0;
                 dN1dy 0 dN2dy 0 dN3dy 0 dN4dy 0;
                   0 dN1dx 0 dN2dx 0 dN3dx 0 dN4dx;
                   0 dN1dy 0 dN2dy 0 dN3dy 0 dN4dy];
end

end