function [  ] = IsingModel( states, CONST, NODES, col )
states = randi([0; 1], length(states),1);
states(states<0.2) = -1;
DE = zeros(length(states),1);
PA = zeros(length(states),1);
initT = 0.10;
Nx = sqrt(length(states)); % number of elements along x
Ny = Nx;
h=patch(NODES.x(CONST.in'),NODES.y(CONST.in'), states);
niter = 1000;
Etot = zeros(niter, 1);
figure;
g = plot(1:1:niter, Etot);
xlabel('Iteration');
ylabel('Energy');
hold off


for iter = 1:niter
    
    T=initT*(niter-iter)/niter;
    for i=1:length(states)
        %period BC
        if i > 0 && i <Nx+1
            up = states(i+Nx);
            down = states(length(states)-Nx+i);
            if i == 1
                left = states(Nx);
                right = states(i+1);
            elseif i == Nx
                left = states(i-1);
                right = states(1);
            else
                left = states(i-1);
                right = states(i+1);
            end
        elseif i>(Nx*(Ny-1)) && i < length(states)+1
            up = states(i-(Nx*(Ny-1)));
            down = states(i-Nx);
            if i == Nx*(Ny-1)+1
                left = (states(end));
                right = states(i+1);
            elseif i == length(states)
                left = states(i-1);
                right = states(Nx*(Ny-1)+1);
            else
                left = states(i-1);
                right = states(i+1);
                
            end
        else
            up = states(i+Nx);
            down =states(i-Nx);
            if any(i == Nx+1:Nx:length(states))
                left = states(i-1+Nx);
                right = states(i+1);
            elseif any(i==Nx:Nx:length(states))
                left = states(i-1);
                right = states(i-Nx+1);
            else
                left = states(i-1);
                right = states(i+1);
            end
            
        end
        DE(i) = 2*states(i)*(left+right+up+down);
            if (exp(-DE(i)/T)>rand(1))
            states(i) = - states(i);
            set(h,'xdata',NODES.x(CONST.in'),'ydata',NODES.y(CONST.in'), 'cdata', states', 'EdgeColor', 'k');
            
            
        end
        title(sprintf('Temp = %.3d   Iter = %d', T, iter));
        drawnow;
    end
    Etot(iter) = -1/4*sum(DE);
    set(g,'xdata', [1:1:iter],'ydata',Etot(1:iter), 'Color', 'k');
    title(sprintf('Temp = %.3d    cia Iter = %d', T, iter));
    drawnow;
    if (length(unique(states))==1)
        break;
    end
end

end


