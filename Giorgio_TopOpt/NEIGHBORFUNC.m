function [VARNEW, breaksearch]=NEIGHBORFUNC(VARNEW,CONST, IsingON, IsingT, SymmetricDesign)
% This function creates a random initial state if VARNEW = []
% otherwise it swap two opposite states which have high probability of
% being swapped accordin to the ISING model
% Code made by Bas Overvelde (overvelde@amolf.nl) and modified by
% Giorgio Oliveri (oliveri@amolf.nl)
if nargin==4
    SymmetricDesign = 0;
end
iter = 0;
itermax = 1000;
breaksearch = 0;

if length(VARNEW)==0    %Draw random initial state
    
    VARNEW=zeros(1,CONST.NumIndepentElem)+CONST.MINDENSITY;
    while sum(VARNEW==1)<round(CONST.Porosity*CONST.NumIndepentElem)
        VARNEW(randperm(CONST.NumIndepentElem,1))=1;
    end
else                    %Pertub state randomly
    % we could put the fllowin 3 lines in while loop
    
    if IsingON == 1
        
          var1 = 1;
          var2 = 1;    
          DE_el1 = 10;
          DE_el2 = 10;
        while (VARNEW(var1)==VARNEW(var2) || exp(-DE_el1/IsingT)<=rand(1) || exp(-DE_el2/IsingT)<=rand(1))
            iter = iter+1;
            if iter ==itermax
                breaksearch = 1;
                break
            end
            
            [var1] = randperm(CONST.NumIndepentElem,1);
            [var2] = randperm(CONST.NumIndepentElem,1);
            DE_el1 = ComputeIsingChangeEnergy( VARNEW, CONST, var1, SymmetricDesign );
            DE_el2 = ComputeIsingChangeEnergy( VARNEW, CONST, var2, SymmetricDesign );

        end  
        if breaksearch
            return
        end
        
        temp = VARNEW(var1);
        VARNEW(var1) = VARNEW(var2);
        VARNEW(var2) = temp;
        
    else
        el1=randperm(CONST.NumIndepentElem,1);
        var1=VARNEW(el1);
        var2=var1;
        while var2==var1
            iter = iter+1;
            el2=randperm(CONST.NumIndepentElem,1);
            var2=VARNEW(el2);

        end
        VAROLD=VARNEW;
        VARNEW(el1)=VAROLD(el2);
        VARNEW(el2)=VAROLD(el1);
    end
    
    
    
    
end

