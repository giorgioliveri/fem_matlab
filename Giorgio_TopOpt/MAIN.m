% Topological Optmization MAIN File
% This code has been made by Giorgio Oliveri PhD student @ AMOLF
% g.oliveri@amolf.nl
close all; clear all; clc;

CONST.AbsoluteConvTol = 1e-5;
CONST.MINDENSITY=1e-5;                    %Density of material without material
CONST.initT=1;                            %Initial temperature (related to acceptance probability)
CONST.Porosity=0.5;                       %Volume fraction or porosity
CONST.NITER=3000;                         %Number of total optimization iterations
CONST.NumElementXUnitCell = 10;           %Number of elements in the Unit Cell (Along x)
CONST.NumElementYUnitCell = 10;           %Number of elements in the Unit Cell (Along y)
CONST.ForceControlled = 1;                %If 1, a vertical force is applied to the upper nodes. Otherwise a displacements is applied.
                                          % this also refers to the meaning of the eigenvalues
                                          

ModePlotting = 0;                         %If 1, the modeshapes are plotted once calculated
CheckBoardStartingConf = 0;

CONST.NumUnitCellX = 2;                   %Number Unit Cell along the horizontal direction
CONST.NumUnitCellY = 10;                  %Number Unit Cell along the vertical direction

SymmetricDesign = 1;                      %If 1 the unit cells are mirrored respect the vertical axis
nModesToCompute = 1;                      %Number of modes to compute

CONST.Lx = 1*CONST.NumUnitCellX;          %Dimension of the whole system (Along x)
CONST.Ly = 1*CONST.NumUnitCellY;          %Dimension of the whole system (Along y)

% MATERIAL PROPERTIES
CONST.E=1;                                %Yound modulus of elements. The emtpy element have same E but their density = 0
CONST.nu=0.49;                            %Poisson Ratio of material element. Empty element have poisson ratio = 0

%INPUT NUMERICAL PROPERTIES
CONST.NX = CONST.NumElementXUnitCell* CONST.NumUnitCellX +1;    %Number of nodes along x
CONST.NY = CONST.NumElementYUnitCell* CONST.NumUnitCellY +1;    %Number of nodes along y
CONST.NEL = (CONST.NX-1)*(CONST.NY-1);                          %Total number of elements
CONST.NumIndepentElem = CONST.NumElementXUnitCell*CONST.NumElementYUnitCell; %Number of elements within the Unit Cell
CONST.tol=1e-6;                           %Tolerance for bounding box to select nodes for Boundary Conditions

AcceptedConfigurationPlotting =1;             %Plot to screen each accepted configuration
IsingON = 1;                              %If = 1, Ising is ACTIVE, with IsingTemp. If = 0 Ising is NOT active
nsample = 30;                             %Number of simulations to run with SAME paramaters
SavingON =1;                              %If = 1 SubDirectory, Datasets and Images are created
IsingTempRange =  [1.5];                  %Ising Temperature to consider
TopOptTempRange = [0.0008];               %Topology Optimization Temperature to consider
PrescribedBucklinRange = [3];

%INITIALIZATION OF PARAMETERS AND EMPTY VECTORS/MATRICES
CONST.Lxe=CONST.Lx/(CONST.NX-1);          %Element size along x
CONST.Lye=CONST.Ly/(CONST.NY-1);          %Element size along y

CONST.in=zeros((CONST.NY-1)*(CONST.NX-1),4);%Preallocation connectivity matrix
CONST.DENS=ones((CONST.NY-1)*(CONST.NX-1),1);%Preallocation density vector

NODES.x=zeros(CONST.NX*CONST.NY,1);         %X coordinate of nodes
NODES.y=NODES.x;                            %Y coordinate of nodes

if CONST.ForceControlled
    %This block define a transformation matrix to relate the DoF of Slave
    %nodes to the Master nodes in the case the load is a vertical force.
    % The master node is the upper left node. The slave nodes are all the
    % remainign nodes in the upper layer
    CONST.slaveDoF = [2*(CONST.NX)*(CONST.NY-1)+4:2:2*CONST.NX*CONST.NY];
    CONST.MasterDoF = 2*(CONST.NX)*(CONST.NY-1)+2;
    for i =1:length(CONST.slaveDoF)
        xv(i) = CONST.slaveDoF(i);
        yv(i) = CONST.MasterDoF;
    end
    xv = [xv 1:1:2*CONST.NX*CONST.NY];
    yv = [yv 1:1:2*CONST.NX*CONST.NY];
    CONST.ToTMasterNodes = setdiff(1:1:2*CONST.NX*CONST.NY,CONST.slaveDoF);    %All independent DoFs
    CONST.TransformationM = sparse(xv, yv, ones(1,length(xv)));
    CONST.TransformationM=CONST.TransformationM(:, CONST.ToTMasterNodes);      %Transformation Matrix
    
end

DataSavingDir =  sprintf('Column_%d_%dx%d/1st_ModeOpt_Tising=[%.2f]_TtopOpt=[%.2f]_BuckForce=[%.2f-%.2f]_in_%d_Points_x_%d_samples/',CONST.NumElementXUnitCell, CONST.NumUnitCellX, CONST.NumUnitCellY, IsingTempRange(1),  TopOptTempRange(1), PrescribedBucklinRange(1), PrescribedBucklinRange(end), length(PrescribedBucklinRange), nsample);
DataSavingName = sprintf('DATA_in_%d_Points_x_%d_samples.mat', length(PrescribedBucklinRange), nsample);

if SavingON
    SavingNumber = 0;
    while exist(DataSavingDir, 'dir')
        SavingNumber = SavingNumber +1;
        DataSavingDir =  sprintf('Column_%d_%dx%d/1st_ModeOpt_Tising=[%.2f]_TtopOpt=[%.2f]_BuckForce=[%.2f-%.2f]_in_%d_Points_x_%d_samples_%d/',CONST.NumElementXUnitCell, CONST.NumUnitCellX, CONST.NumUnitCellY, IsingTempRange(1),  TopOptTempRange(1), PrescribedBucklinRange(1), PrescribedBucklinRange(end), length(PrescribedBucklinRange), nsample, SavingNumber);
    end
    mkdir(DataSavingDir);
end

%Creation nodes
rep=0;
for iy=1:CONST.NY
    for ix=1:CONST.NX
        rep=rep+1;
        NODES.x(rep)=(ix-1)*CONST.Lxe;
        NODES.y(rep)=(iy-1)*CONST.Lye;
    end
end

%Creationg Connectivity Matrix
rep=0;
for iy=1:CONST.NY-1
    for ix=1:CONST.NX-1
        rep=rep+1;
        CONST.in(rep,:)=[(iy-1)*CONST.NX+ix (iy-1)*CONST.NX+ix+1 iy*CONST.NX+ix+1 iy*CONST.NX+ix];
    end
end
%Creation of Material and Void (empty) elements. In this case the
%difference is only the Poisson Ratio equal to CONST.nu and 0 respectively.
%The Yound Modulus is the same for both elements but when assembling the
%global stiffness matrix the density (1 and ~0 respectively) make up for
%the elastic modulus difference.
[CONST.Em,CONST.Em_empty , CONST.B, CONST.G]=ComputeElementMatrices(CONST.Lxe,CONST.Lye,CONST.E,CONST.nu); %COMPUTES ELEMENTS
[CONST.Ke, CONST.Ke_empty]=ElementStiffnessMat(CONST.Lxe,CONST.Lye,CONST.Em,CONST.Em_empty,CONST.B); %LOCAL STIFFNESS MATRIX

% Vector to assemble the Global Stiffness Matrix and Geom Stiff Matrix
rep=0;
CONST.in1=zeros(1,64);
CONST.in2=CONST.in1;
for i=1:8
    for j=1:8
        rep=rep+1;
        CONST.in1(rep)=i;
        CONST.in2(rep)=j;
    end
end

figure;
colormap('gray')
col = ones(CONST.NEL, 1);
axis equal
h=patch(NODES.x(CONST.in'),NODES.y(CONST.in'), col');

%% OPTIMIZATION PART
%Run to initialize maximum energy
globalindex = 0;
DATA = struct('IsingTemperature', cell(1, length(IsingTempRange)*length(TopOptTempRange)), ...
    'TopOptTemperature', cell(1, length(IsingTempRange)*length(TopOptTempRange)), ...
    'ObjectiveFunc', cell(1, length(IsingTempRange)*length(TopOptTempRange)), ...
    'FinalStates', cell(1, length(IsingTempRange)*length(TopOptTempRange)));

isingT = IsingTempRange;
CONST.T = TopOptTempRange;

for PrescribedBucklinIndex = 1:1:length(PrescribedBucklinRange)
    AssignedBuckling = PrescribedBucklinRange(PrescribedBucklinIndex);
    
    for sampleIndex = 1:1:nsample
        globalindex = globalindex+1;
        CONST.EigenRef = 1; %Initialization of Reference Eigenvalue
        RefCase = 1;
        [VAR_REF, breaksearch] =  GetNewCandidate([],CONST, IsingON, isingT, SymmetricDesign, CheckBoardStartingConf, RefCase ); %Obtaining Reference Configuration
        RefCase = 0;
        [~,CONST.EigenRef, ~, ~]=ObjectiveFunction(CONST, NODES, VAR_REF, h, nModesToCompute,AssignedBuckling); %Calculation of reference Eigenvalue for normalization.
        ES=zeros(CONST.NITER,1); %Initialization of Energy Vector
        
        [VARNEW,breaksearch] = GetNewCandidate([],CONST, IsingON, isingT, SymmetricDesign, CheckBoardStartingConf); %Obtaining Initial Starting Configuratio
        [ES(1),DATA(globalindex).Eigenvalues(1,:), ~, ~]=ObjectiveFunction(CONST, NODES, VARNEW, h, nModesToCompute,AssignedBuckling); %Evaluation of Objective Function for starting Configuration
        VAR(1,:)=VARNEW;
        %Permuting unit cell
        for iter=2:CONST.NITER
            tic
            [CNEW, DATA(globalindex).Eigenvalues(iter,:),~, ~]=ObjectiveFunction(CONST, NODES,VARNEW, h,nModesToCompute, AssignedBuckling, ModePlotting);%Determine new objFunc Value
            ENEW=CNEW; %Determine energy according to energy function
            
            %Accept or reject new state
            dF=ENEW-ES(iter-1);    %Determine difference in energy
            PA=exp(-dF/CONST.T);      %Probability to accept
            
            if PA>rand(1) || iter == 2       %Accept
                VAR(iter,:)=VARNEW;       %Save variables for each state
                
                ES(iter)=ENEW;           %Save energy of each state\
                
                if AcceptedConfigurationPlotting %Plot each accepted configuration
                    set(h,'xdata',NODES.x(CONST.in'),'ydata',NODES.y(CONST.in'), 'cdata', -VAR(iter, :)', 'EdgeColor', 'k')
                    axis equal
                    title(sprintf('ObjFunc = %.4d, AssBuck = %.2f', full(CNEW), AssignedBuckling))
                    drawnow
                end
            else                %Reject
                VAR(iter,:)=VAR(iter-1,:);   %Save variables for each state
                ES(iter)=ES(iter-1);        %Save energy for each state
            end
            
            DATA(globalindex).VAR(iter,:)=VAR(iter,:); %Stora last accpted configuration
            [VARNEW,breaksearch]  = GetNewCandidate(VAR(iter, :),CONST, IsingON, isingT, SymmetricDesign, CheckBoardStartingConf);
            
            %Print output
            t1=toc;
            fprintf('ITER = %4d | E = %1.2e | time = %1.2e\n',iter,ES(iter),t1)
            fprintf('\n%0.4f\n', sum(VARNEW)/CONST.NumUnitCellX/CONST.NumUnitCellY/CONST.NumIndepentElem)
            if breaksearch %If Ising can not find a new configuration, break
                fprintf('\nIsing Stopped\n');
                break;
                
            end
            if (ENEW<CONST.AbsoluteConvTol) %If Absolute convergency tolerance met, break
                fprintf('Energy Threshold crossed');
                DATA(globalindex).OptimalSolutionThreshold = 1;
                break;
                
            end
            if iter >1000 %If the objective function is not changed in the last 1000 iter, break
                if all(ES(iter)==ES(iter-1000:iter))
                    fprintf('\nOptimal Solution Reached for 1000 iterations\n');
                    DATA(globalindex).OptimalSolution = 1;
                    break;
                else
                    DATA(globalindex).OptimalSolution = 0;
                end
                
            end
        end
        %plotting and saving last configuration
        set(h,'xdata',NODES.x(CONST.in'),'ydata',NODES.y(CONST.in'), 'cdata', -VAR(iter, :)', 'EdgeColor', 'k')
        axis equal
        pbaspect([1,1,1])
        title(sprintf('ObjFunc = %.3d AssBuck=%.2f Tising= %.2f TtopOpt=%.4f sam = %i', ES(iter), AssignedBuckling, isingT, CONST.T, sampleIndex) )
        drawnow
        figurenamePNG = sprintf('FinalState_Tising=%.2f_TtopOpt=%.4f_AssBucklin=%.2f_sample=%i.png', isingT, CONST.T, AssignedBuckling ,sampleIndex);
        if SavingON
            saveas(gcf, strcat(DataSavingDir, figurenamePNG));
        end
        %saving Result to DATA
        DATA(globalindex).ObjectiveFunc=ES;
        DATA(globalindex).FinalStates=VAR(iter,:);
        DATA(globalindex).FinalObjectiveFunc = ES(iter);
        if breaksearch
            DATA(globalindex).IsingStopped = 1;
        else
            DATA(globalindex).IsingStopped  = 0;
        end
               

        DATA(globalindex).IsingTemperature = isingT;
        DATA(globalindex).TopOptTemperature = CONST.T;
        DATA(globalindex).PrescribedBucklin = PrescribedBucklinRange(PrescribedBucklinIndex);
        DATA(globalindex).Sample = sampleIndex;
        DATA(globalindex).EigenRef = CONST.EigenRef;
    end
    
    
    if any(globalindex == [nsample:nsample:length(PrescribedBucklinRange)*nsample])
        save(strcat(DataSavingDir, DataSavingName), 'DATA', '-v7.3');
    end
    
end

if SavingON
    save(strcat(DataSavingDir, DataSavingName), 'DATA', '-v7.3');
end

